SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

S. Carvajal-Arciniegas<br>
Modelling of salt precipitation and comparison with experimental results <br>
Master's Thesis, 2018<br>
Universität Stuttgart

Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installCarvajal2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Carvajal2018a/raw/master/installCarvajal2018a.sh)
in this folder.

```bash
mkdir -p Carvajal2018a && cd Carvajal2018a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Carvajal2018a/raw/master/installCarvajal2018a.sh
sh ./installCarvajal2018a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installCarvajal2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Carvajal2018a/raw/master/installCarvajal2018a.sh).
