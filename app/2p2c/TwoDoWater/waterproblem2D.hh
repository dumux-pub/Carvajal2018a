// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCMinTests
 * \brief Problem where water is injected in a for flushing precipitated salt clogging a gas reservoir.
 */
#ifndef DUMUX_WATER_PROBLEM2D_HH
#define DUMUX_WATER_PROBLEM2D_HH

#include <cmath>

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/methods.hh>
#include <dumux/discretization/cellcentered/tpfa/properties.hh>
#include <dumux/discretization/box/properties.hh>
#include <dumux/porousmediumflow/2p2c/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

#include <dumux/material/components/granite.hh>
#include <dumux/material/solidsystems/compositionalsolidphase.hh>

#include "waterspatialparams2D.hh"

#include <dumux/io/gnuplotinterface.hh>

namespace Dumux {

template <class TypeTag>
class WaterProblem2D;

namespace Properties {
NEW_TYPE_TAG(WaterTypeTag, INHERITS_FROM(TwoPTwoCNI));
NEW_TYPE_TAG(WaterBoxTypeTag, INHERITS_FROM(BoxModel, WaterTypeTag));
NEW_TYPE_TAG(WaterCCTpfaTypeTag, INHERITS_FROM(CCTpfaModel, WaterTypeTag));

// Set the grid type
//SET_TYPE_PROP(WaterTypeTag, Grid, Dune::YaspGrid<2>);
SET_TYPE_PROP(WaterTypeTag, Grid, Dune::YaspGrid<2, Dune::TensorProductCoordinates<double, 2> >);

// Set the problem property
SET_TYPE_PROP(WaterTypeTag, Problem, WaterProblem2D<TypeTag>);

// Set fluid configuration
SET_PROP(WaterTypeTag, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = FluidSystems::H2OAir<Scalar, Components::H2O<Scalar>>;
};

// Set the spatial parameters
SET_TYPE_PROP(WaterTypeTag, SpatialParams, WaterSpatialparams2D<TypeTag>);

//Set properties here to override the default property settings
SET_INT_PROP(WaterTypeTag, ReplaceCompEqIdx, 3); //!< Replace gas balance by total mass balance
SET_PROP(WaterTypeTag, Formulation)
{ static constexpr auto value = TwoPFormulation::p1s0; };


//! Somerton is used as default model to compute the effective thermal heat conductivity
SET_PROP(WaterTypeTag, ThermalConductivityModel)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using type = ThermalConductivitySomerton<Scalar>;
};

}


// end namespace Properties

/*!
 * \ingroup TwoPNCMinModel
 * \ingroup ImplicitTestProblems
 * \brief Problem where salt water evaporated due to humidity and temperature
 *
 * The domain is sized 0.08m times 0.088m according to lab experiments done at Julich
 * The system is non-isothermal
 * Neumann no-flow boundary condition is applied at the top and right and left side (no flow condition) and Dirichlet conditions is applied at the bottom boundary
 * The model uses mole fractions of dissolved components and volume fractions of precipitated salt as primary variables. Make sure that the according units are used in the problem setup.
 *
 * This problem uses the \ref TwoPNCMinModel.

 */
template <class TypeTag>
class WaterProblem2D : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using SolidSystem = typename GET_PROP_TYPE(TypeTag, SolidSystem);

    enum
    {
        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
        energyEqIdx = Indices::energyEqIdx,
        temperatureIdx = Indices::temperatureIdx,

        // Indices of the components
        H2OIdx = FluidSystem::H2OIdx,
        AirIdx = FluidSystem::AirIdx,

        // Indices of the phases
        liquidPhaseIdx = FluidSystem::liquidPhaseIdx,
        gasPhaseIdx = FluidSystem::gasPhaseIdx,

        // index of the solid phase
        //sPhaseIdx = SolidSystem::comp0Idx,

        // Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx,
        conti1EqIdx = Indices::conti0EqIdx + FluidSystem::AirIdx,

        // Phase State
        bothPhases = Indices::bothPhases,
        firstPhaseOnly = Indices::firstPhaseOnly,
        secondPhaseOnly = Indices::secondPhaseOnly,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };

    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GlobalPosition = typename SubControlVolume::GlobalPosition;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);


public:
    WaterProblem2D(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        containerPressure_      = getParam<Scalar>("Problem.ContainerPressure");
        initLiqSaturation_      = getParam<Scalar>("Problem.LiquidSaturation");
        bottomLiqSaturation_    = getParam<Scalar>("Problem.BottomLiqSaturation");
        AtmosPressure_         = getParam<Scalar>("Problem.AtmosPressure");
        temperatureBL_          = getParam<Scalar>("Problem.TemperatureBL");
        temperatureS_           = getParam<Scalar>("Problem.TemperatureSoilSurface");

        nTemperature_           = getParam<int>("FluidSystem.NTemperature");
        nPressure_              = getParam<int>("FluidSystem.NPressure");
        pressureLow_            = getParam<Scalar>("FluidSystem.PressureLow");
        pressureHigh_           = getParam<Scalar>("FluidSystem.PressureHigh");
        temperatureLow_         = getParam<Scalar>("FluidSystem.TemperatureLow");
        temperatureHigh_        = getParam<Scalar>("FluidSystem.TemperatureHigh");
        name_                   = getParam<std::string>("Problem.Name");

        unsigned int codim = GET_PROP_TYPE(TypeTag, FVGridGeometry)::discMethod == DiscretizationMethod::box ? dim : 0;
        permeability_.resize(fvGridGeometry->gridView().size(codim));

        FluidSystem::init();
    }

    void setTime( Scalar time )
    {
        time_ = time;
    }

    void setTimeStepSize( Scalar timeStepSize )
     {
        timeStepSize_ = timeStepSize;
     }

    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \name Boundary conditions
     */

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        const Scalar rmax = this->fvGridGeometry().bBoxMax()[1];

        // default to Neumann
        bcTypes.setAllNeumann();

        if(globalPos[1] > rmax - eps_)
            bcTypes.setAllDirichlet();

        return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *      boundary segment.
    */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);
        priVars.setState(secondPhaseOnly); //bothPhases

        const Scalar rmax = this->fvGridGeometry().bBoxMax()[1]; 

        if(globalPos[1] > rmax - eps_)
        {
            priVars[pressureIdx]   = AtmosPressure_ ;
            priVars[switchIdx]     = 0.001; // mole fraction of water in air
            priVars[temperatureIdx] = 273.15 + 21;
        }

        return priVars;
    }


//trabajar todo en moles!!

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {

        PrimaryVariables priVars(0.0);
        if (globalPos[1] > 0.096 - eps_){
            priVars.setState(secondPhaseOnly); //secondPhaseOnly

            priVars[switchIdx]   = 0.001;                // mass fraction of H20 in gas phase
            priVars[temperatureIdx] = 273.15 + 21;
            priVars[pressureIdx] = AtmosPressure_;
        }
        else
        {
            priVars.setState(bothPhases);

            priVars[temperatureIdx] = 273.15 + 21;
            priVars[switchIdx]   = initLiqSaturation_;             // saturation of water
            priVars[pressureIdx] = containerPressure_;
        }
        return priVars;
    }

    /*!
     * \name Volume terms
     */
  

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */

    void setGridVariables(std::shared_ptr<GridVariables> gridVariables)
    { gridVariables_ = gridVariables; }

     const GridVariables& gridVariables() const
    { return *gridVariables_; }

    void postTimeStep(const SolutionVector& curSol, Scalar time)
    {
                
        //Scalar evaporation = 0.0;
        Scalar massWater = 0.0;
        if (!(time < 0.0))
        {
            for (const auto& element :  elements(this->fvGridGeometry().gridView()))
            {
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bindElement(element);

                auto elemVolVars = localView(this->gridVariables().curGridVolVars());
                elemVolVars.bindElement(element, fvGeometry, curSol);


                /*for (auto&& scvf : scvfs(fvGeometry))
                {
                    if (scvf.boundary())
                       evaporation += neumann(element, fvGeometry, elemVolVars, scvf)[conti0EqIdx]
                                       * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                }*/

               
                //para imprimir la masa de agua

                for (auto&& scv : scvs(fvGeometry))
                {
                const auto& globalPos = scv.dofPosition();
                if (globalPos[1] < 0.096 + eps_) 
                {
                    const auto& volVars = elemVolVars[scv];
                    
                    for(int liquidPhaseIdx = 0; liquidPhaseIdx <
                    FluidSystem::numPhases; ++liquidPhaseIdx)
                    {
                        
                        massWater += volVars.massFraction(liquidPhaseIdx,
                        FluidSystem::H2OIdx)*volVars.density(liquidPhaseIdx)
                        * scv.volume() * volVars.saturation(liquidPhaseIdx) *
                        volVars.porosity()*volVars.extrusionFactor();
                        
                    }
                }
                }
            }
        }
        // convert to kg/s if using mole fractions
        //evaporation = useMoles ? evaporation * FluidSystem::molarMass(H2OIdx) : evaporation;

        xm_.push_back(time); // in seconds
        ym_.push_back(massWater);

        massplot_.resetPlot();
        massplot_.setXRange(0,std::max(time, 100.));
        massplot_.setYRange(0,std::max(massWater, 5.));
        massplot_.setXlabel("time [s]");
        massplot_.setYlabel("kg");
        massplot_.addDataSetToPlot(xm_, ym_, "massWater");
        massplot_.plot("massWater ");

        //std::cout << "Soil evaporation rate: " << evaporation << " kg/s." << '\n';
        //do a gnuplot
        /*x_.push_back(time); // in seconds
        y2_.push_back(evaporation);

        gnuplot_.resetPlot();
        gnuplot_.setXRange(0,std::max(time, 100.));
        gnuplot_.setYRange(0,std::max(evaporation, 0.0001));
        gnuplot_.setXlabel("time [s]");
        gnuplot_.setYlabel("kg/s");
        gnuplot_.addDataSetToPlot(x_, y2_, "evaporation");
        gnuplot_.plot("evaporation ");*/

    }


    const std::vector<Scalar>& getPermeability()
    {
        return permeability_;
    }

    void updateVtkOutput(const SolutionVector& curSol)
        {
            for (const auto& element : elements(this->fvGridGeometry().gridView()))
            {
                const auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());

                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bindElement(element);

                for (auto&& scv : scvs(fvGeometry))
                {
                    VolumeVariables volVars;
                    volVars.update(elemSol, *this, element, scv);
                    const auto dofIdxGlobal = scv.dofIndex();
                    permeability_[dofIdxGlobal] = volVars.permeability();
                }
            }
        }


    int nTemperature_;
    int nPressure_;
    std::string name_;

    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
    Scalar containerPressure_;
    Scalar AtmosPressure_;
    Scalar temperatureBL_;
    Scalar temperatureS_;
    Scalar initLiqSaturation_;
    Scalar bottomLiqSaturation_;
    Scalar time_ = 0.0;
    Scalar timeStepSize_ = 0.0;
    static constexpr Scalar eps_ = 1e-6;
    std::vector<double> x_;
    std::vector<double> y2_;
    Dumux::GnuplotInterface<double> gnuplot_;
    std::vector<double> xm_;
    std::vector<double> ym_;
    Dumux::GnuplotInterface<double> massplot_;
    std::shared_ptr<GridVariables> gridVariables_;
    std::vector<double> permeability_;
};

} //end namespace Dumux

#endif
