// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCMinTests
 * \brief Spatial parameters for the dissolution problem
 * where water is injected in a for flushing precipitated salt clogging a gas reservoir.
 */
#ifndef DUMUX_WATER_SPATIAL_PARAMETERS_HH
#define DUMUX_WATER_SPATIAL_PARAMETERS_HH

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
//#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/permeabilitykozenycarman.hh>
#include <dumux/porousmediumflow/properties.hh>

namespace Dumux {

/*!
 * \ingroup TwoPNCMinTests
 * \brief Spatial parameters for the dissolution problem
 * where water is injected in a for flushing precipitated salt clogging a gas reservoir.
 */
template<class TypeTag>
class WaterSpatialparams2D
: public FVSpatialParams<typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                         typename GET_PROP_TYPE(TypeTag, Scalar),
                         WaterSpatialparams2D<TypeTag>>
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using GridView = typename FVGridGeometry::GridView;
    static const int dimWorld = GridView::dimensionworld;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;

    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, WaterSpatialparams2D<TypeTag>>;

    //using EffectiveLaw = RegularizedBrooksCorey<Scalar>;
    using EffectiveLaw = RegularizedVanGenuchten<Scalar>;

    using GlobalPosition = typename SubControlVolume::GlobalPosition;

public:
    // type used for the permeability (i.e. tensor or scalar)
    using PermeabilityType = Scalar;
    //! export the material law type used
    using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;

    WaterSpatialparams2D(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {

        sampleHeight_          = 0.096;
        referencePorosity_     = getParam<Scalar>("SpatialParams.referencePorosity");
        referencePermeability_ = getParam<Scalar>("SpatialParams.referencePermeability");
        irreducibleLiqSat_     = getParam<Scalar>("SpatialParams.IrreducibleLiqSat", 0.2);
        irreducibleGasSat_     = getParam<Scalar>("SpatialParams.IrreducibleGasSat", 1e-3);
        /*pEntry1_               = getParam<Scalar>("SpatialParams.Pentry1");
        bcLambda1_             = getParam<Scalar>("SpatialParams.BCLambda1");*/
        vgAlpha_               = getParam<Scalar>("SpatialParams.vgAlpha");
        vgn_                   = getParam<Scalar>("SpatialParams.vgn");

        // residual saturations
        materialParams_.setSwr(irreducibleLiqSat_);
        materialParams_.setSnr(irreducibleGasSat_);

        // parameters of Brooks & Corey Law
        /*materialParams_.setPe(pEntry1_);
        materialParams_.setLambda(bcLambda1_); */
        materialParams_.setVgAlpha(vgAlpha_);
        materialParams_.setVgn(vgn_); 

        // artificial medium parameters 

        ArtifPorosity_     = getParam<Scalar>("SpatialParams.ArtifPorosity");
        ArtifPermeability_ = getParam<Scalar>("SpatialParams.ArtifPermeability");
        /*ArtifpEntry1_      = getParam<Scalar>("SpatialParams.ArtifPEntry1");
        ArtifbcLambda1_    = getParam<Scalar>("SpatialParams.ArtifBcLambda1");*/
        ArtifAlpha_        = getParam<Scalar>("SpatialParams.ArtifAlpha");
        Artifvgn_          = getParam<Scalar>("SpatialParams.Artifvgn");
        ArtifmaterialParams_.setSwr(0.0);
        ArtifmaterialParams_.setSnr(0.8);
        ArtifmaterialParams_.setVgAlpha(ArtifAlpha_);
        ArtifmaterialParams_.setVgn(Artifvgn_); 
        /*ArtifmaterialParams_.setPe(ArtifpEntry1_);
        ArtifmaterialParams_.setLambda(ArtifbcLambda1_);*/
    }

    /*!
     *  \brief Define the minimum porosity \f$[-]\f$ distribution
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar minimalPorosity(const Element& element, const SubControlVolume &scv) const
    { return 1e-5; }

    /*!
     *  \brief Define the reference porosity \f$[-]\f$ distribution.
     *  This is the porosity of the porous medium without any of the
     *  considered solid phases.
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { 
        if (isInArtificial_(globalPos))
            return ArtifPorosity_;
        else
        return referencePorosity_; 
    }

    /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *
     *  Solution dependent permeability function
     */

    Scalar permeabilityAtPos(const GlobalPosition& globalPos) const
    {
        if (isInArtificial_(globalPos))
            return ArtifPermeability_;
        else
        return referencePermeability_;
    }

    // return the brooks-corey context depending on the position
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    { 
        if (isInArtificial_(globalPos))
            return ArtifmaterialParams_;
        else
        return materialParams_; 
    }

    // define which phase is to be considered as the wetting phase
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::H2OIdx; }

private:

    static constexpr Scalar eps_ = 1e-6;

    // provides a convenient way distinguishing whether a given location is inside the aquitard
    bool isInArtificial_(const GlobalPosition &globalPos) const
    {
        // globalPos[dimWorld-1] is the y direction for 2D grids or the z direction for 3D grids
        return globalPos[dimWorld-1] > sampleHeight_ + eps_;
    }

    MaterialLawParams materialParams_;
    MaterialLawParams ArtifmaterialParams_;

    Scalar solubilityLimit_;
    Scalar referencePorosity_;
    Scalar referencePermeability_;
    Scalar irreducibleLiqSat_;
    Scalar irreducibleGasSat_;
    Scalar pEntry1_;
    Scalar bcLambda1_;

    Scalar sampleHeight_;

    Scalar ArtifAlpha_;
    Scalar Artifvgn_;
    Scalar vgAlpha_;
    Scalar vgn_;

    Scalar ArtifpEntry1_;
    Scalar ArtifbcLambda1_; 
    Scalar ArtifPorosity_;
    PermeabilityType ArtifPermeability_;
};

} // end namespace Dumux

#endif
