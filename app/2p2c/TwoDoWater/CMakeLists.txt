dune_symlink_to_source_files(FILES water2D.input)

# isothermal tests
dune_add_test(NAME water2D
              SOURCES waterEva2D.cc
              COMPILE_DEFINITIONS TYPETAG=WaterCCTpfaTypeTag
              COMMAND ${CMAKE_SOURCE_DIR}/bin/testing/runtest.py
              CMD_ARGS      --script fuzzy
                            --files ${CMAKE_SOURCE_DIR}/test/references/saltflushtpfa2pncmin-reference.vtu
                                    ${CMAKE_CURRENT_BINARY_DIR}/water2D-00043.vtu
                            --command "${CMAKE_CURRENT_BINARY_DIR}/water -ParameterFile water2D.input -Problem.Name waterEva2D")

set(CMAKE_BUILD_TYPE Release)

#install sources
install(FILES
waterproblem2D.hh
waterspatialparams2D.hh
waterEva2D.cc
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/Carvajal2018a/app/TwoDoWater/2p2c)
