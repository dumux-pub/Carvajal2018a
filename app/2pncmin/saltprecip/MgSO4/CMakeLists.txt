dune_symlink_to_source_files(FILES saltprecipitationMgSO4.input)

# isothermal tests

dune_add_test(NAME saltprecip_MgSO4
              SOURCES saltprecipitationMgSO4.cc
              COMPILE_DEFINITIONS TYPETAG=SaltprecipitationCCTpfaTypeTag
              COMMAND ${CMAKE_SOURCE_DIR}/bin/testing/runtest.py
              CMD_ARGS      --script fuzzy
                            --files ${CMAKE_SOURCE_DIR}/test/references/saltflushtpfa2pncmin-reference.vtu
                                    ${CMAKE_CURRENT_BINARY_DIR}/salt_MgSO4-00043.vtu
                            --command "${CMAKE_CURRENT_BINARY_DIR}/saltprecip_MgSO4 -ParameterFile saltprecipitationMgSO4.input -Problem.Name salt_MgSO4")

set(CMAKE_BUILD_TYPE Release)

#install sources
install(FILES
saltprecipitationproblem_mgso4.hh
precipitationspatialparams.hh
saltprecipitationMgSO4.cc
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/test/2pncmin/saltprecip/MgSO4)
