// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCMinTests
 * \brief Problem where water is injected in a for flushing precipitated salt clogging a gas reservoir.
 */
#ifndef DUMUX_SALTHETERO_PROBLEM_HH
#define DUMUX_SALTHETERO_PROBLEM_HH

#include <cmath>

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/methods.hh>
#include <dumux/discretization/cellcentered/tpfa/properties.hh>
#include <dumux/discretization/box/properties.hh>
#include <dumux/porousmediumflow/2pncmin/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include "../../../fluidsystems/brineair.hh"

#include <dumux/material/components/nacl.hh>

#include <dumux/material/components/granite.hh>
#include <dumux/material/solidsystems/compositionalsolidphase.hh>

#include "heterospatialparams.hh"

#include <dumux/io/gnuplotinterface.hh>

namespace Dumux {

template <class TypeTag>
class SaltheteroProblem;

namespace Properties {
NEW_TYPE_TAG(SaltheteroTypeTag, INHERITS_FROM(TwoPNCMinNI));
NEW_TYPE_TAG(SaltheteroBoxTypeTag, INHERITS_FROM(BoxModel, SaltheteroTypeTag));
NEW_TYPE_TAG(SaltheteroCCTpfaTypeTag, INHERITS_FROM(CCTpfaModel, SaltheteroTypeTag));

// Set the grid type
//SET_TYPE_PROP(SaltheteroTypeTag, Grid, Dune::YaspGrid<2>);
SET_TYPE_PROP(SaltheteroTypeTag, Grid, Dune::YaspGrid<2, Dune::TensorProductCoordinates<double, 2> >);

// Set the problem property
SET_TYPE_PROP(SaltheteroTypeTag, Problem, SaltheteroProblem<TypeTag>);

// Set fluid configuration
SET_PROP(SaltheteroTypeTag, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = FluidSystems::BrineAir<Scalar, Components::H2O<Scalar>>;
};

SET_PROP(SaltheteroTypeTag, SolidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using ComponentOne = Components::NaCl<Scalar>;
    using ComponentTwo = Components::Granite<Scalar>;
    static constexpr int numInertComponents = 1;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, ComponentOne, ComponentTwo, numInertComponents>;
};

// Set the spatial parameters
SET_TYPE_PROP(SaltheteroTypeTag, SpatialParams, saltHeteroSpatialparams<TypeTag>);

//Set properties here to override the default property settings
SET_INT_PROP(SaltheteroTypeTag, ReplaceCompEqIdx, 3); //!< Replace gas balance by total mass balance
SET_PROP(SaltheteroTypeTag, Formulation)
{ static constexpr auto value = TwoPFormulation::p1s0; };


//! Somerton is used as default model to compute the effective thermal heat conductivity
SET_PROP(SaltheteroTypeTag, ThermalConductivityModel)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using type = ThermalConductivitySomerton<Scalar>;
};

}


// end namespace Properties

/*!
 * \ingroup TwoPNCMinModel
 * \ingroup ImplicitTestProblems
 * \brief Problem where salt water evaporated due to humidity and temperature
 *
 * The domain is sized 0.08m times 0.088m according to lab experiments done at Julich
 * The system is non-isothermal
 * Neumann no-flow boundary condition is applied at the top and right and left side (no flow condition) and Dirichlet conditions is applied at the bottom boundary
 * The model uses mole fractions of dissolved components and volume fractions of precipitated salt as primary variables. Make sure that the according units are used in the problem setup.
 *
 * This problem uses the \ref TwoPNCMinModel.

 */
template <class TypeTag>
class SaltheteroProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using SolidSystem = typename GET_PROP_TYPE(TypeTag, SolidSystem);

    enum
    {
        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
        energyEqIdx = Indices::energyEqIdx,
        temperatureIdx = Indices::temperatureIdx,

        // component indices
        // TODO: using xwNaClIdx as privaridx works here, but
        //       looks like magic. Can this be done differently??
        xwNaClIdx = FluidSystem::NaClIdx,
        precipNaClIdx = FluidSystem::numComponents,

        // Indices of the components
        H2OIdx = FluidSystem::H2OIdx,
        AirIdx = FluidSystem::AirIdx,
        NaClIdx = FluidSystem::NaClIdx,

        // Indices of the phases
        liquidPhaseIdx = FluidSystem::liquidPhaseIdx,
        gasPhaseIdx = FluidSystem::gasPhaseIdx,

        // index of the solid phase
        sPhaseIdx = SolidSystem::comp0Idx,


        // Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx,
        precipNaClEqIdx = Indices::conti0EqIdx + FluidSystem::numComponents,

        // Phase State
        bothPhases = Indices::bothPhases,
        firstPhaseOnly = Indices::firstPhaseOnly,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };

    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GlobalPosition = typename SubControlVolume::GlobalPosition;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);

public:
    SaltheteroProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        salinity_               = getParam<Scalar>("Problem.Salinity");

        containerPressure_      = getParam<Scalar>("Problem.ContainerPressure");
        initLiqSaturation_      = getParam<Scalar>("Problem.LiquidSaturation");
        bottomLiqSaturation_    = getParam<Scalar>("Problem.BottomLiqSaturation");
        bottomPressure_         = getParam<Scalar>("Problem.BottomPressure");
        temperatureBL_          = getParam<Scalar>("Problem.TemperatureBL");
        temperatureS_           = getParam<Scalar>("Problem.TemperatureSoilSurface");

        nTemperature_           = getParam<int>("FluidSystem.NTemperature");
        nPressure_              = getParam<int>("FluidSystem.NPressure");
        pressureLow_            = getParam<Scalar>("FluidSystem.PressureLow");
        pressureHigh_           = getParam<Scalar>("FluidSystem.PressureHigh");
        temperatureLow_         = getParam<Scalar>("FluidSystem.TemperatureLow");
        temperatureHigh_        = getParam<Scalar>("FluidSystem.TemperatureHigh");
        name_                   = getParam<std::string>("Problem.Name");

        unsigned int codim = GET_PROP_TYPE(TypeTag, FVGridGeometry)::discMethod == DiscretizationMethod::box ? dim : 0;
        permeability_.resize(fvGridGeometry->gridView().size(codim));

        FluidSystem::init();
    }

    void setTime( Scalar time )
    {
        time_ = time;
    }

    void setTimeStepSize( Scalar timeStepSize )
     {
        timeStepSize_ = timeStepSize;
     }

    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.

    Scalar temperature() const
    { return temperature_; } */

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        const Scalar rmin = this->fvGridGeometry().bBoxMin()[1];

        // default to Neumann
        bcTypes.setAllNeumann();

        // Constant pressure at bottom
        if(globalPos[1] < rmin + eps_)
            bcTypes.setAllDirichlet();

        return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *      boundary segment.
    */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);
        priVars.setState(firstPhaseOnly); //bothPhases

        const Scalar rmin = this->fvGridGeometry().bBoxMin()[1];

        if(globalPos[1] < rmin + eps_)
        {
            priVars[pressureIdx]   = bottomPressure_ ;
            priVars[switchIdx]     = bottomLiqSaturation_; // Saturation inner boundary
            priVars[xwNaClIdx]     = massToMoleFrac_(salinity_);// mole fraction salt
            priVars[precipNaClIdx] = 0.0;
            priVars[temperatureIdx] = 273.15 + 21;
        }

        return priVars;
    }


//trabajar todo en moles!!

    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
   {

        PrimaryVariables values(0.0);
        const auto& globalPos = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];

        const Scalar rmax = this->fvGridGeometry().bBoxMax()[1];

        const Scalar boundaryLayerThickness = 0.005;
        Scalar moleFracH2o_inter= volVars.moleFraction(gasPhaseIdx, H2OIdx);
//std::cout<<"mole frac "<<moleFracH2o_inter<<std::endl;
        Scalar moleFracH2o_BL = 0.0097;
        Scalar temperatureS_ = volVars.temperature();

        if(globalPos[1] > rmax - eps_)
        {

            // evaporation
            Scalar evaporationSurf = volVars.density(gasPhaseIdx)
                                         * volVars.diffusionCoefficient(gasPhaseIdx, H2OIdx)
                                         * (moleFracH2o_inter - moleFracH2o_BL)
                                         / boundaryLayerThickness;
//std::cout<<"evaporation rate "<<evaporationSurf<<std::endl;
//std::cout<<"density air "<< volVars.density(gasPhaseIdx)<<std::endl;

            values[conti0EqIdx] = useMoles ? evaporationSurf : evaporationSurf * FluidSystem::molarMass(H2OIdx);

            //values[conti0EqIdx] = evaporationSurf;

            //energy
            values[energyEqIdx] = 0.0;
            values[energyEqIdx] = FluidSystem::componentEnthalpy(volVars.fluidState(), gasPhaseIdx, H2OIdx) * values[conti0EqIdx] * FluidSystem::molarMass(H2OIdx);
            values[energyEqIdx] += FluidSystem::thermalConductivity(elemVolVars[scvf.insideScvIdx()].fluidState(), gasPhaseIdx) * (temperatureS_ - temperatureBL_)/boundaryLayerThickness;

        }

        return values;
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables priVars(0.0);
        priVars.setState(bothPhases);

        priVars[pressureIdx] = containerPressure_;
        priVars[switchIdx]   = initLiqSaturation_;                 // Sw primary variable
        priVars[xwNaClIdx]   = massToMoleFrac_(salinity_);     // mole fraction
        priVars[temperatureIdx] = 273.15 + 21;
        priVars[precipNaClIdx] = 0.0; // [kg/m^3]

        return priVars;
    }

    /*!
     * \name Volume terms
     */


    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param values The source and sink values for the conservation equations in units of
     *                 \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The subcontrolvolume
     *
     * For this method, the \a values parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    NumEqVector source(const Element &element,
                   const FVElementGeometry& fvGeometry,
                   const ElementVolumeVariables& elemVolVars,
                   const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);
        const auto& globalPos = scv.dofPosition();
        const auto& volVars = elemVolVars[scv];


        Scalar moleFracNaCl_wPhase = volVars.moleFraction(liquidPhaseIdx, NaClIdx);
        Scalar moleFracNaCl_nPhase = volVars.moleFraction(gasPhaseIdx, NaClIdx);
        Scalar massFracNaCl_Max_wPhase = this->spatialParams().solubilityLimit();
        Scalar moleFracNaCl_Max_wPhase = massToMoleFrac_(massFracNaCl_Max_wPhase);
        Scalar moleFracNaCl_Max_nPhase = moleFracNaCl_Max_wPhase / volVars.pressure(gasPhaseIdx);
        Scalar saltPorosity = this->spatialParams().minimalPorosity(element, scv);

        // liquid phase
        using std::abs;
        Scalar precipSalt = volVars.porosity() * volVars.molarDensity(liquidPhaseIdx)
                                               * volVars.saturation(liquidPhaseIdx)
                                               * abs(moleFracNaCl_wPhase - moleFracNaCl_Max_wPhase);
        if (moleFracNaCl_wPhase < moleFracNaCl_Max_wPhase)
            precipSalt *= -1;

        // gas phase
        precipSalt += volVars.porosity() * volVars.molarDensity(gasPhaseIdx)
                                         * volVars.saturation(gasPhaseIdx)
                                         * abs(moleFracNaCl_nPhase - moleFracNaCl_Max_nPhase);

        // make sure we don't dissolve more salt than previously precipitated
        if (precipSalt*timeStepSize_ + volVars.solidVolumeFraction(sPhaseIdx)* volVars.solidComponentMolarDensity(sPhaseIdx)< 0)
            precipSalt = -volVars.solidVolumeFraction(sPhaseIdx)* volVars.solidComponentMolarDensity(sPhaseIdx)/timeStepSize_;

        if (globalPos[0] < 0.0425 + eps_){

        if (volVars.solidVolumeFraction(sPhaseIdx) >= this->spatialParams().referencePorosity(element, scv) - saltPorosity  && precipSalt > 0)
            precipSalt = 0;

        return source;
        }else{

        if (volVars.solidVolumeFraction(sPhaseIdx) >= this->spatialParams().addPorosity(element, scv) - saltPorosity  && precipSalt > 0)
            precipSalt = 0;

        }
        source[conti0EqIdx + NaClIdx] += -precipSalt;
        source[precipNaClEqIdx] += precipSalt;

        return source;

    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */


    void setGridVariables(std::shared_ptr<GridVariables> gridVariables)
    { gridVariables_ = gridVariables; }

     const GridVariables& gridVariables() const
    { return *gridVariables_; }

    void postTimeStep(const SolutionVector& curSol, Scalar time)
    {
        Scalar evaporation = 0.0;
        Scalar massWater = 0.0;
        if (!(time < 0.0))
        {
            for (const auto& element :  elements(this->fvGridGeometry().gridView()))
            {
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bindElement(element);

                auto elemVolVars = localView(this->gridVariables().curGridVolVars());
                elemVolVars.bindElement(element, fvGeometry, curSol);

                for (auto&& scvf : scvfs(fvGeometry))
                {
                    if (scvf.boundary())
                       evaporation += neumann(element, fvGeometry, elemVolVars, scvf)[conti0EqIdx]
                                       * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                }

                //para imprimir la masa de agua
                for (auto&& scv : scvs(fvGeometry))
                {
                const auto& globalPos = scv.dofPosition();
                if (globalPos[1] < 0.096)
                {
                    const auto& volVars = elemVolVars[scv];
                    for(int liquidPhaseIdx = 0; liquidPhaseIdx <
                    FluidSystem::numPhases; ++liquidPhaseIdx)
                    {
                        massWater += volVars.massFraction(liquidPhaseIdx,
                        FluidSystem::H2OIdx)*volVars.density(liquidPhaseIdx)
                        * scv.volume() * volVars.saturation(liquidPhaseIdx) *
                        volVars.porosity() * volVars.extrusionFactor();
                    }

                }
                }
            }
        }
        // convert to kg/s if using mole fractions

        xm_.push_back(time); // in seconds
        ym_.push_back(massWater);

        massplot_.resetPlot();
        massplot_.setXRange(0,std::max(time, 100.));
        massplot_.setYRange(0,std::max(massWater, 5.));
        massplot_.setXlabel("time [s]");
        massplot_.setYlabel("kg");
        massplot_.addDataSetToPlot(xm_, ym_, "massWater");
        massplot_.plot("massWater ");

        //do a gnuplot

        evaporation = useMoles ? evaporation * FluidSystem::molarMass(H2OIdx) : evaporation;
        std::cout << "Soil evaporation rate: " << evaporation << " kg/s." << '\n';

        x_.push_back(time); // in seconds
        y2_.push_back(evaporation);

        gnuplot_.resetPlot();
        gnuplot_.setXRange(0,std::max(time, 100.));
        gnuplot_.setYRange(0, 2e-7);
        gnuplot_.setXlabel("time [s]");
        gnuplot_.setYlabel("kg/s");
        gnuplot_.addDataSetToPlot(x_, y2_, "evaporation");
        gnuplot_.plot("evaporation ");

    }



    const std::vector<Scalar>& getPermeability()
    {
        return permeability_;
    }

    void updateVtkOutput(const SolutionVector& curSol)
        {
            for (const auto& element : elements(this->fvGridGeometry().gridView()))
            {
                const auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());

                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bindElement(element);

                for (auto&& scv : scvs(fvGeometry))
                {
                    VolumeVariables volVars;
                    volVars.update(elemSol, *this, element, scv);
                    const auto dofIdxGlobal = scv.dofIndex();
                    permeability_[dofIdxGlobal] = volVars.permeability();
                }
            }
        }

private:

    /*!
     * \brief Returns the molality of Na2SO4 (mol Na2SO4 / kg water) for a given mole fraction
     *
     * \param XwNa2SO4 the XwNa2SO4 [kg Na2SO4 / kg solution]
     */
    static Scalar massToMoleFrac_(Scalar XwNaCl)
    {
       const Scalar Mw = 18.015e-3; //FluidSystem::molarMass(H2OIdx); /* molecular weight of water [kg/mol] */ //TODO use correct link to FluidSyswem later
       const Scalar Ms = 58.44e-3;  //FluidSystem::molarMass(NaClIdx); /* molecular weight of NaCl  [kg/mol] */

       const Scalar X_NaCl = XwNaCl;
       /* XwNa2SO4: conversion from mass fraction to mol fraction */
       auto xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
       return xwNaCl;
    }

    int nTemperature_;
    int nPressure_;
    std::string name_;

    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
    Scalar salinity_;
    Scalar containerPressure_;
    Scalar bottomPressure_;
    Scalar temperatureBL_;
    Scalar temperatureS_;
    Scalar initLiqSaturation_;
    Scalar bottomLiqSaturation_;
    Scalar time_ = 0.0;
    Scalar timeStepSize_ = 0.0;
    static constexpr Scalar eps_ = 1e-6;
    std::vector<double> x_;
    std::vector<double> y2_;
    Dumux::GnuplotInterface<double> gnuplot_;
    std::vector<double> xm_;
    std::vector<double> ym_;
    Dumux::GnuplotInterface<double> massplot_;
    std::shared_ptr<GridVariables> gridVariables_;
    std::vector<double> permeability_;
};

} //end namespace Dumux

#endif
