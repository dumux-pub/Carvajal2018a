// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief Material properties of pure salt \f$Na2SO4\f$.
 */
#ifndef DUMUX_NA2SO4_HH
#define DUMUX_NA2SO4_HH

#include <dumux/common/exceptions.hh>

#include <cmath>
#include <iostream>

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>
#include <dumux/material/components/solid.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the Na2SO4 properties
 */
template <class Scalar>
class Na2SO4
: public Components::Base<Scalar, Na2SO4<Scalar> >
, public Components::Solid<Scalar, Na2SO4<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the Na2SO4.
     */
    static std::string name()
    {
        return "Na2SO4";
    }

    /*!
     * \brief The molar mass of Na2SO4 in \f$\mathrm{[kg/mol]}\f$.
     */
    static Scalar molarMass()
    {
        return 200.04e-3 ; //CRC Handbook 142.04e-3
    }

    /*!
     * \brief The mass density \f$\mathrm{[kg/m^3]}\f$ of Na2SO4.
     */
    static Scalar solidDensity(Scalar temperature)
    {
        return 2700.0; //CRC Handbook
    }

    /*!
     * \brief The mass density \f$\mathrm{[kg/m^3]}\f$ of NaCl.
     */
    static Scalar solidMolarDensity(Scalar temperature)
    {
        return solidDensity(temperature)/molarMass();
    }

    /*!
     * \brief The specific heat capacity \f$\mathrm{[J/molK]}\f$ of Na2SO4.
     */
    static Scalar solidHeatCapacity(Scalar temperature)
    {
        return 128.2; //from https://en.wikipedia.org/wiki/Sodium_sulfate_(data_page)
    }

    /*!
     * \brief Thermal conductivity of the component \f$\mathrm{[W/(m*K)]}\f$ as a solid.
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     */
    static Scalar solidThermalConductivity(Scalar temperature)
    {
        return 3.61; //to change
    }


};

} // end namespace Components

} // end namespace Dumux

#endif
